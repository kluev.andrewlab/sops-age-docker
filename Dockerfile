FROM bash:alpine3.14

LABEL org.opencontainers.image.authors="Andrew Kluev <kluev.andrew@gmail.com>"

ENV SOPS_AGE_KEY_FILE=/age.key

RUN wget -O "/tmp/age.tar.gz" "https://github.com/FiloSottile/age/releases/download/v1.0.0-rc.3/age-v1.0.0-rc.3-linux-amd64.tar.gz" \
    && wget -O "/usr/bin/sops" "https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux" \
    && tar -xzf "/tmp/age.tar.gz" \
    && mv "./age" "/usr/bin/age" \
    && chmod +x "/usr/bin/age" \
    && chmod +x "/usr/bin/sops" \
    && rm -rf "/tmp/age" \
    && rm -rf "https://github.com/FiloSottile/age/releases/download/v1.0.0-rc.3/age-v1.0.0-rc.3-linux-amd64.tar.gz" \
    && rm -rf "https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux"

COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY sopsctl.sh /usr/local/bin/sopsctl
RUN chmod +x "/docker-entrypoint.sh" && chmod +x "/usr/local/bin/sopsctl"

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["sopsctl"]
