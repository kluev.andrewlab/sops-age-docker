USAGE:

```shell
# Encrypt files:
docker run --rm -it -e AGE_KEY_B64=<BASE64_OF_AGE_KEY_FILE> -v <ABSOULTE_DIRECTORY_WITH_FILES>:/files kluevnadrew/sops-age /files ecnrypt
docker run --rm -it -e AGE_KEY_B64=<BASE64_OF_AGE_KEY_FILE> -v <ABSOULTE_DIRECTORY_WITH_FILES>:/files kluevnadrew/sops-age /files e
docker run --rm -it -v <ABSOULTE_AGE_KEY_FILE_PATH>/age.key -v <ABSOULTE_DIRECTORY_WITH_FILES>:/files kluevnadrew/sops-age /files e

# Decrypt files:
docker run --rm -it -e AGE_KEY_B64=<BASE64_OF_AGE_KEY_FILE> -v <ABSOULTE_DIRECTORY_WITH_FILES>:/files kluevnadrew/sops-age /files decrypt
docker run --rm -it -e AGE_KEY_B64=<BASE64_OF_AGE_KEY_FILE> -v <ABSOULTE_DIRECTORY_WITH_FILES>:/files kluevnadrew/sops-age /files d
docker run --rm -it -v <ABSOULTE_AGE_KEY_FILE_PATH>/age.key -v <ABSOULTE_DIRECTORY_WITH_FILES>:/files kluevnadrew/sops-age /files d
```

LICENSE:
![wtfpl](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)
