#!/usr/bin/env bash

function encrypt {
  sops -e -i --age "$(grep 'public' "$SOPS_AGE_KEY_FILE" | awk '{ print $4 }')" "$1"
}

function encrypt_all {
    find "$1" -name "*.enc" -print0 | while read -r -d $'\0' file
      do
        decrypt "$file" 2>/dev/null
        if encrypt "$file"; then
          echo "$file encrypted"
        fi
      done
    exit 0
}

function decrypt {
  sops --decrypt --output "$file" "$file"
}

function decrypt_all {
    find "$1" -name "*.enc" -print0 | while read -r -d $'\0' file
      do
        if decrypt "$file"; then
          echo "$file decrypted"
        fi
      done
    exit 0
}

function help {
    echo "Usage $0 {directory} {command}"
    echo ""
    echo "Command"
    echo "e|encrypt -- Encrypt all *.enc files in directory"
    echo "d|decrypt -- Decrypt all *.enc files in directory"
    echo "h|help    -- Prints this help"
}

case "$2" in
  "e") encrypt_all "$1";;
  "encrypt") encrypt_all "$1";;
  "d") decrypt_all "$1";;
  "decrypt") decrypt_all "$1";;
  "h")
      help
      exit 0
      ;;
  "help")
      help
      exit 0
      ;;
  *)
      help
      exit 1
      ;;
esac


